# schemetran

SCHEMETRAN is a rudimentary (scheme) interpreter in Fortran 2018.
It's not intended for production use, although this attitude may change if at least one person on Earth needs it for his work.
It leaks symbols (but not pairs).

! A rudimentary scheme interpreter
! This interpreter was only implemented as an exercise 5.51 of the SICP
! It has he following issues:
! Leaks everything that is (read)
! Leaks symbols
! Leaks labels
! (It shouldn't leak strings though.)
! Has no obarray, so symbol equality is done in O(n) => slow
! The garbage collector seems to be working though, and everything
! above can be fixed/improved if at least one person on Earth needs it.
! Writing it took me about 104 hours.
! I didn't know any Fortran at all before.
! I found 3 gfortran bugs while writing this:
! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94471
! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94380
! https://gcc.gnu.org/pipermail/fortran/2020-April/054173.html

Compile with make
